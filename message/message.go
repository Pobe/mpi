package message

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mpi/mpiType"
)

const (
	//Used to finish read on any message
	StopCharacter = "\r\n\r\n"
)

//Message.Route enum
const (
	RouteDefault = iota //0
	RouteSend    = iota
	RouteGather  = iota
	RouteReduce  = iota
	RouteType    = iota
	RouteEndComm = iota
	RouteBarrier = iota
)

//Message make use of marshalling/unmarshalling json serialization
type Message struct {
	Destination int         `json:"destination"` //rank of process
	Source      int         `json:"source"`      //rank of process
	Datatype    uint        `json:"datatype"`
	Route       uint        `json:"route"` //used for routing on communicator side
	Body        interface{} `json:"body"`
	Tag         string      `json:"tag"`
}

//Matcher struct is received by a process and look for a message that match with it
type Matcher struct {
	Datatype uint   `json:"datatype"`
	Source   int    `json:"source"`
	Route    uint   `json:"route"`
	Tag      string `json:"tag"`
}

func Decode(connByteMessage []byte) (Message, error) {
	var response Message
	err := json.Unmarshal(connByteMessage, &response)

	//Casting
	if response.Body != nil {
		switch response.Datatype {
		case mpiType.MPI_String:
			response.Body = response.Body.(string)
		case mpiType.MPI_Int:
			response.Body = int(response.Body.(float64))
		default:
			fmt.Println("Could not cast")
			fmt.Println(response.Body)
			os.Exit(1)
		}
	}

	return response, err
}

func Encode(message Message) ([]byte, error) {
	return json.Marshal(message)
}

func DecodeMatcher(connByteMatcher []byte) (Matcher, error) {
	var response Matcher
	err := json.Unmarshal(connByteMatcher, &response)
	return response, err
}

func EncodeMatcher(match Matcher) ([]byte, error) {
	return json.Marshal(match)
}

func (m *Matcher) IsMatch(mRecv Message) bool {
	match := m.Datatype == mRecv.Datatype && m.Source == mRecv.Source
	if m.Route > 0 {
		match = m.Route == mRecv.Route
	}
	//TODO tag
	//if m.Tag != nil && match {
	// match := m.Tag == mRecv.Tag
	// }
	return match
}
