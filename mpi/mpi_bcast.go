package mpi

func MPI_Bcast(data *interface{}, datatype uint, root int, CommWorld commWorld) {
	if MPI_Comm_rank() == root {
		for i := 0; i < MPI_Comm_size(); i++ {
			if i != root {
				MPI_Send(data, datatype, i, CommWorld)
			}
		}
	} else {
		MPI_Recv(data, datatype, root, CommWorld)
	}
}
