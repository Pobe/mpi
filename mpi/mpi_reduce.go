package mpi

import "fmt"

//Operation enum
const (
	MPI_SUM = iota
)

func MPI_Reduce(local *float64, global *float64, operation uint, root int, CommWorld commWorld) {
	if CommWorld.rank != root {
		var interf interface{}
		interf = local
		MPI_Send(&interf, MPI_Int, root, CommWorld)
	} else {
		size := CommWorld.commSize
		//Build empty result array
		result := make([]float64, size)

		//Get results
		var localResult interface{}
		for rank := 0; rank < size; rank++ {
			if rank != root {
				MPI_Recv(&localResult, MPI_Int, rank, CommWorld)
				result[rank] = float64(localResult.(int))
			}
		}

		result[root] = *local
		fmt.Println(result)

		switch operation {
		case MPI_SUM:
			*global = opSum(result)
		default:
			fmt.Println("Error on reduce operation")
		}
	}
}

func opSum(arr []float64) float64 {
	total := 0.0
	for i := 0; i < len(arr); i++ {
		total += arr[i]
	}

	return total
}
