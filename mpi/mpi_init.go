package mpi

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"gitlab.com/mpi/mpiType"
)

const (
	host = "localhost"
)

//Datatypes
const (
	MPI_String = mpiType.MPI_String
	MPI_Int    = mpiType.MPI_Int
)

type commWorld struct {
	rank         int
	commPort     int
	commProcPort int
	commSize     int
}

//CommWorld global mpi variable representing the world and process
//communication information
var CommWorld commWorld

//MPI_Init initialize mpi for the given script
func MPI_Init() {
	rank, _ := strconv.Atoi(os.Getenv("MPIGO_RANK"))
	commPort, _ := strconv.Atoi(os.Getenv("MPIGO_COMM_PORT"))
	commProcPort, _ := strconv.Atoi(os.Getenv("MPIGO_PROC_PORT"))
	commSize, _ := strconv.Atoi(os.Getenv("MPIGO_COMM_SIZE"))

	CommWorld = commWorld{
		rank:         rank,
		commPort:     commPort,
		commProcPort: commProcPort,
		commSize:     commSize,
	}

	commReady := isReady(commPort)
	procsReady := isReady(commPort + commSize) //ensure last proc has been started
	if !commReady {
		fmt.Println("Could not connect to communicator.")
	}

	if !procsReady {
		fmt.Println("Could not connect to last process.")
	}
}

func isReady(commPort int) bool {
	ready := false
	maxTry := 3
	try := 0
	for try < maxTry && !ready {
		conn, err := net.Dial("tcp", host+":"+strconv.Itoa(commPort))
		if err != nil {
			fmt.Println("Waiting for communicator.")
			try++
			time.Sleep(1 * time.Second)
		} else {
			ready = true
			defer conn.Close()
		}
	}
	return ready
}

func MPI_Comm_size() int {
	return CommWorld.commSize
}

func MPI_Comm_rank() int {
	return CommWorld.rank
}

func MPI_Get_processor_name() string {
	return host
}

func MPI_Finalize() {
	//TODO, se rebrancher avec mpirun fermer le process.
	//Script finalization is managed by mpirun. This should just check if port is closed.
}
