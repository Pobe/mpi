package mpi

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"gitlab.com/mpi/message"
)

const (
	//Millisecond
	maxTimeToSync = 60000
	pollingCheck  = time.Second
)

func MPI_Barrier(CommWorld commWorld) {
	conn, err := net.Dial("tcp", host+":"+strconv.Itoa(CommWorld.commPort))
	if err != nil {
		fmt.Println("Could not connect to communicator")
	}

	messageToSend := message.Message{
		Route:       message.RouteBarrier,
		Destination: CommWorld.rank,
	}

	encodedMessage, err := message.Encode(messageToSend)
	if err != nil {
		fmt.Println("Cannot encode barrier message")
		os.Exit(1)
	}
	conn.Write(encodedMessage)

	//wait
	wait(CommWorld)
}

func wait(CommWorld commWorld) {
	matchThis := message.Matcher{
		Route:  message.RouteBarrier,
		Source: CommWorld.rank,
	}

	waitForMessage(matchThis, host, strconv.Itoa(CommWorld.commProcPort), maxTimeToSync, pollingCheck)
}
