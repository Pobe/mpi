package mpi

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"gitlab.com/mpi/message"
)

//TODO changer le data pour intertace{} et manipuler le tout avec l'encode decode
//De message
//Faire aussi les type pour permettre le décodage
func MPI_Send(data *interface{}, datatype uint, destination int, CommWorld commWorld) {
	conn, err := net.Dial("tcp", host+":"+strconv.Itoa(CommWorld.commPort))
	if err != nil {
		fmt.Println("Could not connect to communicator")
	}

	messageToSend := message.Message{
		Destination: destination,
		Source:      CommWorld.rank,
		Datatype:    datatype,
		Body:        *data,
		Route:       message.RouteSend,
	}

	encodedMessage, err := message.Encode(messageToSend)
	if err != nil {
		fmt.Println("Cannot encode message : ", *data)
		os.Exit(1)
	}
	conn.Write(encodedMessage)
}

func MPI_Recv(data *interface{}, datatype uint, source int, CommWorld commWorld) {
	matchThis := message.Matcher{
		Datatype: datatype,
		Source:   source,
		Route:    message.RouteDefault,
	}

	decodedMessage := waitForMessage(matchThis, host, strconv.Itoa(CommWorld.commProcPort), 100, time.Millisecond)

	*data = decodedMessage.Body
}

//waitForMessage is blocking, and waits for a proper response to give a feedback
func waitForMessage(matcher message.Matcher, host string, port string, tryLimit int, polling time.Duration) message.Message {
	var recvMessage message.Message
	maxTry := tryLimit
	try := 0

	for recvMessage == (message.Message{}) && try < maxTry {
		conn, err := net.Dial("tcp", host+":"+port)
		if err != nil {
			fmt.Println("Could not connect to process comm")
			fmt.Println(err)
		}
		defer conn.Close()

		encodedMatch, err := message.EncodeMatcher(matcher)
		if err != nil {
			fmt.Println("Cannot encode matcher")
		}
		_, err = conn.Write(encodedMatch)

		if err != nil {
			fmt.Println("Could not write to process")
		}

		buffer := make([]byte, 1024)
		responseLen, err := conn.Read(buffer)
		if err != nil {
			fmt.Println(err)
			fmt.Println("Error problem reading while polling")
		}
		recvMessage, err = message.Decode(buffer[:responseLen])
		try++
		time.Sleep(polling) //as fast as possible
	}

	if recvMessage == (message.Message{}) {
		fmt.Println("Error failed getting message")
		os.Exit(1)
	}
	return recvMessage
}
