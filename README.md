# mpirun
Script servant à découvrir les hosts et démarrer mpi
Définit le nombre de processus

Envoie le nom du répertoire où il a été invoquer sur la machine à chaque noeud.

Exécute le script en question sur chaque host.
Connector.

`mpirun -proc=<number of processes> <program name and arguments>`
