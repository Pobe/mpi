package communicator

import (
	"fmt"
	"math/rand"
	"net"
	"os"
	"strconv"
	"sync"
	"time"

	"gitlab.com/mpi/message"
	"gitlab.com/mpi/proc"
)

const (
	ConnHost = "localhost"
	ConnType = "tcp"
)

//MPI_Barrier enum
const (
	idle      = iota
	waiting   = iota
	responded = iota
)

func randomPort() int {
	rand.Seed(time.Now().UTC().UnixNano())
	return 3000 + rand.Intn(400)
}

var ConnPort = randomPort()
var barrierSync = &sync.Mutex{}

//MpiCommWorld wraps the pool in which processes communicate together
//Finalize ensures each processes has called MPI_Finalize before shutting down COMM_WORLD
type MpiCommWorld struct {
	ProcList []*proc.Proc
	Port     int
	ProcPort int
	Finalize chan struct{}
	Barrier  *[]int
}

func CreateWorld(nbProc int) *MpiCommWorld {

	procBarrierIdx := make([]int, nbProc)
	mpiCommWorld := MpiCommWorld{
		ProcList: make([]*proc.Proc, nbProc),
		Port:     ConnPort,
		ProcPort: ConnPort + 1,
		Barrier:  &procBarrierIdx,
	}

	go mpiCommWorld.startComm()
	return &mpiCommWorld
}

func (mcw *MpiCommWorld) startComm() {
	listener := createListener(strconv.Itoa(mcw.Port))
	defer listener.Close()
	fmt.Println("Communicator listening on " + ConnHost)

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		//This has to be blocking. No go routine!
		handleRequest(mcw, conn)
	}
}

func createListener(port string) net.Listener {
	listener, err := net.Listen(ConnType, ConnHost+":"+port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	return listener
}

func handleRequest(mcw *MpiCommWorld, conn net.Conn) {
	buf := make([]byte, 2048)
	msgLen, err := conn.Read(buf)
	if err != nil {
		//Empty dial
		// fmt.Println("Empty dial")
		return
	}

	messageObj, err := message.Decode(buf[:msgLen])
	if err == nil {
		// fmt.Println("Message Forwarded to " + strconv.Itoa(messageObj.Destination))
		mcw.ForwardMessage(messageObj)
	} else {
		fmt.Println(err)
		fmt.Println("Message `" + string(buf[:]) + "` not forwarded")
	}
	conn.Close()
}

//ForwardMessage transfers a message receive from a process to an other
//A process sends a message to COMM_WORLD, and COMM_WORLD has the responsability the
//forward it
//This has to be an array since message a source handling, tag or type
func (mcw *MpiCommWorld) ForwardMessage(message message.Message) {
	(*mcw.ProcList[message.Destination].MessageList) = append(
		(*mcw.ProcList[message.Destination].MessageList), message)
}

//ProcListener Start a new process and binds it to COMM_WORLD so it knowns to which process
//to forward messages to
func (mcw *MpiCommWorld) ProcListener(rank int, finalizeChan chan int) {
	port := mcw.ProcPort + rank
	p := proc.Proc{
		Rank:         rank,
		Port:         port,
		MessageList:  &([]message.Message{}),
		FinalizeChan: finalizeChan,
	}

	procListener := createListener(strconv.Itoa(port))
	defer procListener.Close()
	//Bind process to communicator
	mcw.ProcList[rank] = &p

	fmt.Println("Process listening on " + ConnHost + ":" + strconv.Itoa(port))

	//This has to be none blocking
	go p.Exec(mcw.Port, len(mcw.ProcList))

	for {
		conn, err := procListener.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		go handleProcRequest(conn, p.MessageList, mcw)
	}
}

func handleProcRequest(conn net.Conn, messageList *[]message.Message, mcw *MpiCommWorld) {
	buf := make([]byte, 1024)
	msgLen, err := conn.Read(buf)

	if err != nil {
		// fmt.Println("Empty proc call")
		return
	}

	matcher, err := message.DecodeMatcher(buf[:msgLen])

	if err != nil {
		fmt.Println("Could not decode matcher")
	}

	var success bool
	for idx, m := range *messageList {
		//Routing
		success = false
		switch m.Route {
		case message.RouteBarrier:
			success = routeBarrier(conn, matcher, mcw)
		case message.RouteSend, message.RouteDefault:
			success = routeRecv(conn, m, matcher)
		default:
			fmt.Println("Could not route message")
			conn.Write([]byte(message.StopCharacter))
			return
		}
		//Drop message
		if success {
			*messageList = append((*messageList)[:idx], (*messageList)[idx+1:]...)
			return
		}
	}
	//return empty
	conn.Write([]byte(message.StopCharacter))
}

func routeRecv(conn net.Conn, m message.Message, matcher message.Matcher) bool {
	if matcher.IsMatch(m) {
		encodedMessage, err := message.Encode(m)
		if err != nil {
			fmt.Println("Could not encode message for process")
			return false
		}
		conn.Write(encodedMessage)
		return true
	}
	return false
}

func routeBarrier(conn net.Conn, matcher message.Matcher, mcw *MpiCommWorld) bool {
	success := false
	barrierSync.Lock()
	barrierIdx := (*mcw.Barrier)[matcher.Source]

	if matcher.Route == message.RouteBarrier {
		//set waiting mode
		if barrierIdx == idle {
			(*mcw.Barrier)[matcher.Source] = waiting
		} else if barrierIdx == waiting && sum(*mcw.Barrier) >= waiting*len(*mcw.Barrier) {
			(*mcw.Barrier)[matcher.Source] = responded
			encodedMessage, err := message.Encode(message.Message{
				//return message to self
				Destination: matcher.Source,
				Tag:         "Barrier",
			})
			if err != nil {
				fmt.Println("Could not encode message for process")
			}
			conn.Write(encodedMessage)
			success = true
		}

		if sum(*mcw.Barrier) == responded*len(*mcw.Barrier) {
			//Everyone has received their response
			//Reset for future barrier
			for idx := range *mcw.Barrier {
				(*mcw.Barrier)[idx] = idle
			}
		}

	}
	barrierSync.Unlock()
	return success
}

//TODO put this in util package
func sum(arr []int) int {
	result := 0
	for _, v := range arr {
		result += v
	}
	return result
}
