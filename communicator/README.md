The communicator acts as orted in OpenMPI.

It's the gateway to communication between processes serializing data between them.

It's using zeromq to implement its functionalities.

This needs to be started has a daemon. `mpirun` will bind with it on initiation to make sure the node can be used with mpi.

Each process on a node have it's own queue to receive message.

Each process sends a message to the communicator. The communicator forwards it to the proper process. The communicator keeps track of COMM_WORLD.

Multiple COMM_WORLD can be started for a single node.

# API
/create_world [name] [size]
Called by mpirun on each host at the very begin

