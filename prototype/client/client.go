package main

import (
	"fmt"
	"net"
)

func main() {
	conn, _ := net.Dial("tcp", "localhost:1111")
	defer conn.Close()

	conn.Write([]byte("My message"))

	buffer := make([]byte, 1024)
	l, _ := conn.Read(buffer)
	fmt.Println(string(buffer[:l]))
}
