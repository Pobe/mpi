package main

import (
	"fmt"
	"net"
)

func main() {
	listener, _ := net.Listen("tcp", "localhost:1111")
	defer listener.Close()

	list := []int{1, 2}
	for {
		conn, _ := listener.Accept()
		go handle(conn, list)
	}
}

func handle(conn net.Conn, l []int) {
	buf := make([]byte, 1024)
	msgLen, err := conn.Read(buf)
	if err != nil {
		fmt.Println("error reading")
	}

	msg := string(buf[:msgLen])

	conn.Write([]byte(msg + "WOOO"))
}
