package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/mpi/mpi"
)

func main() {
	//No need for argc argv with go
	mpi.MPI_Init()

	size := mpi.MPI_Comm_size()
	rank := mpi.MPI_Comm_rank()
	processor := mpi.MPI_Get_processor_name()

	fmt.Printf("Hello from %s, rank %d, out of %d. My pid is %d\n", processor, rank, size, os.Getpid())

	if size != 4 {
		fmt.Println("Need 4 process to make that work.")
		os.Exit(1)
	}

	var aWord interface{}
	var aInt interface{}
	if rank == 0 {
		aWord = "Hello from sender 0!"
		mpi.MPI_Send(&aWord, mpi.MPI_String, 1, mpi.CommWorld)
	} else if rank == 1 {
		mpi.MPI_Recv(&aWord, mpi.MPI_String, 0, mpi.CommWorld)
		fmt.Println("Rank 1 just received this message : " + aWord.(string))
	} else if rank == 2 {
		aInt = 987654
		mpi.MPI_Send(&aInt, mpi.MPI_Int, 3, mpi.CommWorld)
	} else {
		mpi.MPI_Recv(&aInt, mpi.MPI_Int, 2, mpi.CommWorld)
		fmt.Println("Rank 3 just received this int : " + strconv.Itoa(aInt.(int)))
	}

	mpi.MPI_Finalize()
}
