package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"gitlab.com/mpi/mpi"
)

func main() {
	//No need for argc argv with go
	mpi.MPI_Init()

	if mpi.MPI_Comm_size() != 8 {
		fmt.Println("Need 8 process to make that work.")
		os.Exit(1)
	}

	rank := mpi.MPI_Comm_rank()
	if rank == 0 {
		fmt.Println("p:" + strconv.Itoa(rank) + ", " + string(time.Now().String()) + " : Going to take a coffee")
		time.Sleep(time.Second * 4)
	} else {
		fmt.Println("p:" + strconv.Itoa(rank) + ", " + string(time.Now().String()) + " : waiting")
	}

	mpi.MPI_Barrier(mpi.CommWorld)
	fmt.Println("p:" + strconv.Itoa(rank) + ", " + string(time.Now().String()) + " : ended")

	mpi.MPI_Finalize()
}
