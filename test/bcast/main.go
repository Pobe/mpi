package main

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/mpi/mpi"
)

func main() {
	mpi.MPI_Init()

	if mpi.MPI_Comm_size() != 8 {
		fmt.Println("Need 8 processors to work")
		os.Exit(1)
	}
	var phrase interface{}
	if mpi.MPI_Comm_rank() == 3 {
		phrase = "Je suis le process 3 et je broadcast"
	}
	mpi.MPI_Bcast(&phrase, mpi.MPI_String, 3, mpi.CommWorld)
	if mpi.MPI_Comm_rank() != 3 {
		fmt.Println("p:" + strconv.Itoa(mpi.MPI_Comm_rank()) + " phrase : " + phrase.(string))
	}
}
