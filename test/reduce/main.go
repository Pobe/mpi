package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"

	"gitlab.com/mpi/mpi"
)

const (
	arrSize = 10
)

func random(min, max float64) float64 {
	rand.Seed(time.Now().Unix())
	return float64(rand.Intn(int(max)-int(min)) + int(min))
}

func main() {
	mpi.MPI_Init()

	if mpi.MPI_Comm_size() != 10 {
		fmt.Println("Need 10 processes")
		os.Exit(1)
	}

	var local float64

	//40
	lastDigit := float64(mpi.MPI_Comm_rank())
	arr := [arrSize]float64{1, 2, 3, 4, 4, 5, 8, 9, 4, lastDigit}

	// for i := 0; i < len(arr); i++ {
	// arr[i] = random(1, 15)
	// }

	local = sum(arr)
	fmt.Println("Local sum : " + strconv.Itoa(int(local)))

	//Reduce
	var global float64
	mpi.MPI_Reduce(&local, &global, mpi.MPI_SUM, 0, mpi.CommWorld)

	if mpi.MPI_Comm_rank() == 0 {
		fmt.Printf("Total sum = %f, avg = %f\n",
			global, global/float64((mpi.MPI_Comm_size()*len(arr))))
	}

}

func sum(arr [arrSize]float64) float64 {
	total := 0.0
	for i := 0; i < len(arr); i++ {
		total += arr[i]
	}
	return total
}
