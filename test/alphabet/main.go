package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/mpi/mpi"
)

func main() {
	mpi.MPI_Init()
	alphabetStart := 97

	if mpi.MPI_Comm_size() != 2 {
		fmt.Println("2 process only to tell the alphabet.")
		os.Exit(1)
	}

	alphabetCount := 0
	var letter interface{}
	for alphabetCount < 26 {
		if mpi.MPI_Comm_rank() == 0 {
			letter = string(byte(alphabetCount + alphabetStart))
			mpi.MPI_Send(&letter, mpi.MPI_String, 1, mpi.CommWorld)
			fmt.Printf("p:%d saying %s\n", mpi.MPI_Comm_rank(), letter)
		} else {
			mpi.MPI_Recv(&letter, mpi.MPI_String, 0, mpi.CommWorld)
			fmt.Printf("p:%d yelling %s\n", mpi.MPI_Comm_rank(), strings.ToUpper(letter.(string)))
		}
		alphabetCount++
	}

	mpi.MPI_Finalize()
}
