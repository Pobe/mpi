BASE = test/base
ALPHABET = test/alphabet
BARRIER = test/barrier
BCAST = test/bcast
REDUCE = test/reduce

all: build_test build_mpirun run

build_mpirun:
	cd mpirun && go build

run:
	@echo "\nBASE\n-----------------------------------"
	cd mpirun && ./mpirun -proc=4 ../$(BASE)/base
	@echo "\nALPHABET\n-----------------------------------"
	cd mpirun && ./mpirun -proc=2 ../$(ALPHABET)/alphabet
	@echo "\nBARRIER\n-----------------------------------"
	cd mpirun && ./mpirun -proc=8 ../$(BARRIER)/barrier
	@echo "\nBCAST\n-----------------------------------"
	cd mpirun && ./mpirun -proc=8 ../$(BCAST)/bcast
	@echo "\nREDUCE\n-----------------------------------"
	cd mpirun && ./mpirun -proc=10 ../$(REDUCE)/reduce

#Tests
build_test: base alphabet barrier bcast reduce

base:
	cd $(BASE) && go build
alphabet:
	cd $(ALPHABET) && go build
barrier:
	cd $(BARRIER) && go build
bcast:
	cd $(BCAST) && go build
reduce:
	cd $(REDUCE) && go build
