package proc

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"gitlab.com/mpi/message"
)

const (
	BaseShell = "/bin/bash"
)

//Proc represent an independent process
type Proc struct {
	Rank         int
	Pid          int
	Port         int
	MessageList  *[]message.Message
	FinalizeChan chan int
}

func (p *Proc) Exec(commWorldPort int, size int) {
	cmd := exec.Command(BaseShell, "-c", strings.Join(os.Args[:], " "))
	cmd.Env = append(os.Environ(),
		"MPIGO_RANK="+strconv.Itoa(p.Rank),
		"MPIGO_COMM_PORT="+strconv.Itoa(commWorldPort),
		"MPIGO_PROC_PORT="+strconv.Itoa(p.Port),
		"MPIGO_COMM_SIZE="+strconv.Itoa(size),
	)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println("Error can't start process ", err.Error())
	}
	fmt.Println(string(stdoutStderr[:]))
	p.FinalizeChan <- p.Rank
}
