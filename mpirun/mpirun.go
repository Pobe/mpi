package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"gitlab.com/mpi/communicator"
)

func main() {
	nbProc := flag.Int("proc", 1, "Number of processes")

	flag.Parse()
	//tail
	flagArgs := flag.Args()

	if len(flagArgs) < 1 {
		fmt.Println("ERROR: Needs an executable in arguments")
		os.Exit(1)
	}

	executable := flagArgs[0]
	execPath, err := os.Executable()
	if err != nil {
		fmt.Println("Error can't get executable")
		os.Exit(1)
	}

	//relpath
	if string(executable[0]) != "/" {
		dirExecPath := filepath.Dir(execPath)

		//to abspath
		flagArgs[0] = dirExecPath + "/" + executable
	}

	//Replace os.Args for processes
	os.Args = flagArgs

	comm := communicator.CreateWorld(*nbProc)
	fmt.Println("Starting processes")
	finalizeChan := make(chan int, *nbProc)
	for rank := 0; rank < *nbProc; rank++ {
		go comm.ProcListener(rank, finalizeChan)
	}

	//Wait
	endedCount := 0
ENDED:
	for {
		select {
		case rank := <-finalizeChan:
			fmt.Println("Process " + strconv.Itoa(rank) + " is done.")
			endedCount++
			if endedCount == *nbProc {
				break ENDED
			}
		case <-time.After(60 * time.Second):
			fmt.Println("Seems like a process hanged. Forcing exit.")
			os.Exit(1)
		}
	}
	os.Exit(0)
}
